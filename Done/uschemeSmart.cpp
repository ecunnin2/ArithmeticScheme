#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <unistd.h>
#include <memory>

using namespace std;

bool BATCH = false;
bool DEBUG = false;


struct Node {
    Node(string value, shared_ptr<Node> left=nullptr, shared_ptr<Node> right=nullptr);
  //  Node(string value, shared_ptr<Node> leftPtr, shared_ptr<Node> rightPtr);
    ~Node();
    
    string value;
    struct shared_ptr<Node> left;
    struct shared_ptr<Node> right;
    
    friend ostream &operator<<(ostream &os, const Node &n);
};

Node::Node(string newValue, shared_ptr<Node> newLeft, shared_ptr<Node> newRight){
    value = newValue;
    left = newLeft;
    right = newRight;
}
Node::~Node(){
}
ostream &operator<<(ostream &os, const Node &n) {
    if(n.left){
        os << "(Node: value=" <<n.value <<", left=" << *n.left <<", right=" << *n.right <<")";
    }
    else{
        os << "(Node: value=" <<n.value <<")";
    }
    
    return os;
}



string parse_token(istream &s){
    string token;
    while(s.peek() ==' '){
        s.get();}
    token = s.get();
    if (token == "(" || token == ")" || token == "+" || token == "-" || token == "*" || token == "/"){
        return token;}
    else {
        while(s.peek() >= '0' && s.peek() <= '9')
            token.push_back(s.get());
    }
    return token;
}

shared_ptr<Node> parse_expression(istream &s){
    string token;
    token = parse_token(s);
    shared_ptr<Node> left = nullptr;
    shared_ptr<Node> right = nullptr;
    if (token == "" || token == ")"){
        return nullptr;}
    else if(token == "("){
        token = parse_token(s);
        left = parse_expression(s);
        if(left){
            right = parse_expression(s);
        }
        if(right){
            parse_token(s);
        }
    }
    shared_ptr<Node> node (new Node(token));
    node->left = left;
    node->right = right;
    return node;
}

//Evaluation
//------------------------------------------
//


shared_ptr<Node> evaluate(shared_ptr<Node> t){
    shared_ptr<Node> t1=nullptr;
    shared_ptr<Node> t2=nullptr;
    if (t){
        shared_ptr<Node> t1=evaluate(t->left);
        shared_ptr<Node> t2=evaluate(t->right);
        if (t1 && t2){
            if (t->value.compare("+")==0)
                return shared_ptr<Node> (new Node(to_string(stoi(t1->value)+stoi(t2->value))));
            if (t->value.compare("-")==0)
                return shared_ptr<Node> (new Node(to_string(stoi(t1->value)-stoi(t2->value))));
            if (t->value.compare("*")==0)
                return shared_ptr<Node> (new Node(to_string(stoi(t1->value)*stoi(t2->value))));
            if (t->value.compare("/")==0)
                return shared_ptr<Node> (new Node(to_string(stoi(t1->value)/stoi(t2->value))));
        }
        else{
            return shared_ptr<Node> (new Node(t->value));
        }
    }
    else{
        return 0;
    }
}

//Main Execution
//-----------------------------------
//

int main(int argc, char *argv[]) {
    string line;
    int c;
    
    while ((c = getopt(argc, argv, "bdh")) != -1) {
        switch (c) {
            case 'b': BATCH = true; break;
            case 'd': DEBUG = true; break;
            default:
                cerr << "usage: " << argv[0] << endl;
                cerr << "    -b Batch mode (disable prompt)"   << endl;
                cerr << "    -d Debug mode (display messages)" << endl;
                return 1;
        }
    }
    
    while (!cin.eof()) {
        if (!BATCH) {
            cout << ">>> ";
            cout.flush();
        }
        
        if (!getline(cin, line)) {
            break;
        }
        
        if (DEBUG) { cout << "LINE: " << line << endl; }
        
        stringstream s(line);
        shared_ptr<Node> n = parse_expression(s);
        //if (DEBUG) { cout << "TREE: " << *n << endl; }
        //cout <<*n <<endl;
        shared_ptr<Node> result = evaluate(n);
        cout <<result->value <<endl;
        
    }
    
    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
//
