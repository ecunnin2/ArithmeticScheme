/*  Project01: Arithmetic Scheme
 *  Contributors: jcastro2, ecunnin2, twray1
 *  Date: Feb. 7, 2017
 */

#include <iostream>
#include <sstream>
#include <stack>
#include <string>
#include <unistd.h>

using namespace std;

bool BATCH = false;
bool DEBUG = false;


struct Node {
    Node(string value, Node *left=nullptr, Node *right=nullptr);
    ~Node();
    string value;
    Node * left;
    Node * right;
    
    friend ostream &operator<<(ostream &os, const Node &n);
};
Node::Node(string newValue, Node * newLeft, Node * newRight){
    value = newValue;
    left = newLeft;
    right = newRight;
}
Node::~Node(){
    delete left;
    delete right;
}
ostream &operator<<(ostream &os, const Node &n) {
    if(n.left){
        os << "(Node: value=" <<n.value <<", left=" << *n.left <<", right=" << *n.right <<")";
    }
    else{
        os << "(Node: value=" <<n.value <<")";
    }
    
    return os;
}



string parse_token(istream &s){
    string token;
    while(s.peek() ==' '){
        s.get();}
    token = s.get();
    if (token == "(" || token == ")" || token == "+" || token == "-" || token == "*" || token == "/"){
        return token;}
    else {
        while(s.peek() >= '0' && s.peek() <= '9')
            token.push_back(s.get());
    }
    return token;
}

Node *parse_expression(istream &s){
    string token;
    token = parse_token(s);
    Node * left = nullptr;
    Node * right = nullptr;
    if (token == "" || token == ")"){
        return nullptr;}
    else if(token == "("){
        token = parse_token(s);
        left = parse_expression(s);
        if(left){
            right = parse_expression(s);
        }
        if(right){
            parse_token(s);
        }
    }
    return new Node(token,left,right);

}

//Evaluation
//------------------------------------------
//



//This function receives a node of the syntax tree
//and recursively evaluates it
int evaluate(Node* temp)
{
    //empty the tree is empty
    if (!temp)
        return 0;
    
    //leaf node i.e, an integer
    if (!temp->left && !temp->right)
        return stoi((temp->value));
    
    //Evaluate left subtree
    int left = evaluate(temp->left);
    
    //Evaluate right subtree
    int right = evaluate(temp->right);
    
    //Check which operator to apply
    if (temp->value=="+")
        return left+right;
    
    if (temp->value=="-")
        return left-right;
    
    if (temp->value=="*")
        return left*right;
    
    return (left/right);
}



//Main Execution
//-----------------------------------
//

int main(int argc, char *argv[]) {
    string line;
    int c;
    
    while ((c = getopt(argc, argv, "bdh")) != -1) {
        switch (c) {
            case 'b': BATCH = true; break;
            case 'd': DEBUG = true; break;
            default:
                cerr << "usage: " << argv[0] << endl;
                cerr << "    -b Batch mode (disable prompt)"   << endl;
                cerr << "    -d Debug mode (display messages)" << endl;
                return 1;
        }
    }
    
    while (!cin.eof()) {
        if (!BATCH) {
            cout << ">>> ";
            cout.flush();
        }
        
        if (!getline(cin, line)) {
            break;
        }
        
        if (DEBUG) { cout << "LINE: " << line << endl; }
        
        stringstream s(line);
        Node *n = parse_expression(s);
        if (DEBUG) { cout << "TREE: " << *n << endl; }
        int eval  = evaluate(n);
        cout << eval <<endl;
        
        delete n;
    }
    
    return 0;
}

// vim: set sts=4 sw=4 ts=8 expandtab ft=cpp:
//
