#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <stack>
#include <string>
#include <vector>

using namespace std;

struct Node{
	string value;
	struct Node * left;
	struct Node * right;


	Node(string value):value(value), left(nullptr), right(nullptr){
		
	}
};

template <typename T>
class BinaryTree{
	
	protected:
		Node * root;


	public:
		BinaryTree<T>(){};
		~BinaryTree<T>(){};
		BinaryTree(const BinaryTree<T> &source);
		BinaryTree<T>& operator=(const BinaryTree<T> &source);
	
		friend ostream& operator<<(ostream& os, BinaryTree& bt){
			bt.display();
		}

		
		void insert(vector<string> &expression){
			stack<Node *> symbols;

			for(vector<string>::iterator it=expression.begin(); it<expression.end(); it++){
				string dig = *it;
				if(!dig.compare("(")){
				
				}else if(!dig.compare(")")){
					Node * tempRight = symbols.top();
					symbols.pop();
					Node * tempLeft = symbols.top();
					symbols.pop();
					Node * tempHead = symbols.top();
					symbols.pop();
					tempHead->left = tempLeft;
					tempHead->right = tempRight;
					symbols.push(tempHead);
				}else{
					Node * temp = new Node(dig);
					symbols.push(temp);
				}
			}
			root = symbols.top();
		}

		Node * getRoot(){
			return root;
		}
		void display(){
			print(root);
		}
		void print(Node *t){
    			if(t){
				if(!(t))
        				return;
        			cout << t->value << endl;
				if(!(t))
        				return;
				print(t->left);
				print(t->right);
    			}
		}

		Node * evaluate(Node *t){
			Node *t1=nullptr;
			Node *t2=nullptr;
			if (t){
				Node *t1=evaluate(t->left);
				Node *t2=evaluate(t->right);
				if (t1 && t2){
					if (t->value.compare("+")==0)
						return new Node(to_string(stoi(t1->value)+stoi(t2->value)));
					if (t->value.compare("-")==0)
						return new Node(to_string(stoi(t1->value)-stoi(t2->value)));
					if (t->value.compare("*")==0)
						return new Node(to_string(stoi(t1->value)*stoi(t2->value)));
					if (t->value.compare("/")==0)
						return new Node(to_string(stoi(t1->value)/stoi(t2->value)));
				}
				else{
					return new Node(t->value);
				}
			}else{
				return 0;
			}
		}
};

int main(){
	vector<string> expression;
	char c;


	string words;
	getline(cin, words);
	string temp = "";
	for(char c:words){
		if(c != ' '){
			temp = temp + c;
			expression.push_back(temp);
			temp = "";
		}
	}	
	/* Debugging
	for (auto it = expression.begin(); it != expression.end(); ++it)
	{
		cout <<*it <<endl;
	}
	*/
	BinaryTree<Node> tree;
	tree.insert(expression);
	//cout <<tree;
	cout<<(tree.evaluate(tree.getRoot()))->value<<endl;
	return 0;
}
