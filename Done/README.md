1. N Consists of all the numbers and operators that are in the mathematical expression because both operators and numbers get their own node. Since each node has to be visited once, the time complexity of our interpreter in terms of Big-O notation is O(n). The impact of using a stack during the evaluation is that it allows for a recursive evaluation of the expression, as we can keep track of a value and then return to it later when the function returns from a call. We didn't explicitly utilize a stack class to keep track of numbers and operators, rather we used the fact that recursion already creates memory stacks, so we placed return statements in such a way that returning values end up where they belong within a mathematical expression, which resulted in one final number.


2. The uschemeSmart executable is larger by about 23Kb, however, it uses less memory since the pointers are shared.


3. We would prefer to use the smart pointers, because with them, memory allocation is not a pain, and we do not have to worry about dealing with memory leaks, although the file size is slightly larger, the increase is not large enough to create that much of a drawback. Smart pointers win out undoubtedly.

4. The entire project, mainly th structure and interpreter are set up with the assumption that that each sub operation is only between two numbers. This prompted us to organize a binary tree. If many different operandsare using the same operator, then a regular tree will be required. The Node structure would have a the ability to create multiple branches instead of just two. The Parser would need a new way to indicate the end of a sub expression, and the interpreter would need to be updated to continue to interpret and evalue until the indicator is reahed, instead of only taking two digits at a time. Overall, many changes would need to be made to support the new arbitrary length.

Each member of the put in an equal amount of time. Initially we planned to divide up each section, but when several errors were reached we needed to all work on the one section of the code to have more minds trying to solve the issue. Overall, it was a great effort from each of the members.

Joel put in a good effort, and I would definitely want to work with him on future projects. When the code was not performing the way it was supposed to, he did not shy away.

Trent put in a good effort as well and would be a greate future teamate. When the code was not performing the way it was supposed to, he did not shy away.
